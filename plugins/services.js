
import slider from '~/services/slider'
import expedition from '~/services/expedition'
import teams from '~/services/teams'

export default function ({ $axios }, inject) {
  const services = {
    slider: slider($axios),
    expedition: expedition($axios),
    teams: teams($axios)
  }

  Object.keys(services).forEach(function (key) {
    inject(key, services[key])
  })
}