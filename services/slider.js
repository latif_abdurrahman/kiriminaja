export default function ($axios) {
  const http = $axios

  return {
    get: function () {
      const getUrl = '/slider'
      return http.get(getUrl)
    },
  };
}
