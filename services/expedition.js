export default function ($axios) {
  const http = $axios

  return {
    get: function () {
      const getUrl = '/expedition'
      return http.get(getUrl)
    },
  };
}