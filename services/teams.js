export default function ($axios) {
  const http = $axios

  return {
    get: function () {
      const getUrl = '/teams'
      return http.get(getUrl)
    },
  };
}