export const state = () => ({
  listSlide: []
})

export const mutations = {
  add(state, slide) {
    slide.forEach(index => {
      state.listSlide.push(index)
    })
  },
  updateSlide(state, data) {
    state.listSlide = []
    data.forEach(index => {
      state.listSlide.push(index)
    })
  }
}