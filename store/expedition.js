export const state = () => ({
  listExpeditions: []
})

export const mutations = {
  add(state, data) {
    data.forEach(index => {
      state.listExpeditions.push(index)
    })
  },
  updateExpedition(state, data) {
    state.listExpeditions = []
    data.forEach(index => {
      state.listExpeditions.push(index)
    })
  }
}