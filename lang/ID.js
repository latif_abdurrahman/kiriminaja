export default {
  navbar : {
    beranda: "Beranda",
    blog: "Blog",
    kabar: "Kabar",
    layanan: "Layanan",
    cekongkir: "Cek Ongkir",
    lacak: "Lacak",
    daftar: "Daftar",
    masuk: "Masuk"
  },
  carousel: {
    selengkapnya: "Selengkapnya"
  },
  commons : {
    submit: "Submit",
    recordnotfound: "Sorry data is not available.",
    recordtitle: "Not found",
  },
  services: {
    header: "Untuk Semua Kebutuhan Pengiriman Paketmu",
  },
  benefit: {
    header: "Kirim Paket Makin Singkat",
    subheader: "Masih kurang? Yuk explore sekarang biar ngga penasaran"
  }
}